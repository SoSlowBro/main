package game.atmo;
import game.atmo.GasMix.Gas;
import openfl.errors.*;
import Direction;

import TemporarySupport;
/**
 * ...
 * @author Bo
 */
class GasController3D
{
	private static var SwitchDirs:Array<Int> = [Direction.X_N, Direction.Y_N, Direction.Z_N];
	// checkin time << 1;
	
	private var mixes:Array<Array<Array<GasMix>>>;
	private var sizeX:Int;
	private var sizeY:Int;
	private var sizeZ:Int;
	
	/**
	 * Constructor for gas controller
	 * @param	sizeX	amount of mixtures by X coordinate
	 * @param	sizeY	amount of mixtures by Y coordinate
	 * @param	sizeZ	amount of mixtures by Z coordinate
	 */
	public function new(sizeX:Int = 200, sizeY:Int = 200, sizeZ:Int = 20) //TODO replase args with normal points.
	{
		mixes = [];
		this.sizeX = sizeX; this.sizeY = sizeY; this.sizeZ = sizeZ;
		var airPreset:GMPreset = new GMPreset(GMPreset.MainPresets.DEFAULT);
		var spacePreset:GMPreset = new GMPreset(GMPreset.MainPresets.SPACE);
		
		if (sizeX < 1)
			throw new ArgumentError("sizeX can't be less than 1");
		if (sizeY < 1)
			throw new ArgumentError("sizeY can't be less than 1");
		if (sizeZ < 1)
			throw new ArgumentError("sizeY can't be less than 1");
		
		for (i in 0...sizeX)
		{
			mixes[i] = [];
			for (j in 0...sizeY) 
			{
				mixes[i][j] = [];
				for (k in 0...sizeZ) 
				{
					mixes[i][j].push( new GasMix() );
					mixes[i][j][k].setPreset( (Math.random() > 0.5 ? airPreset : spacePreset) );
				}
			}
		}
	}
	
	/**
	 * Performs main calculation between stored mixtures.
	 */
	public function process():Void {
		var bitflag:Int;
		var adj:GasMix = null;
		var gm:GasMix = null;
		for (i in 0...sizeX)
		{
			for (j in 0...sizeY) 
			{
				for (k in 0...sizeZ) 
				{
					gm = mixes[i][j][k];
					
					if ( gm.get_changed() && ! gm.get_blocked() )
					{
						bitflag = getAdjacentMixes(i,j,k);
						
							for (dir in SwitchDirs) // Обходим все направления
							{
								if (dir & bitflag > 0) {
									switch dir
									{
										/*
										case Direction.X_P:
											adj = mixes[i + 1][j][k];
										*/
										case Direction.X_N:
											adj = mixes[i - 1][j][k];
										/*
										case Direction.Y_P:
											adj = mixes[i][j + 1][k];
										*/
										case Direction.Y_N:
											adj = mixes[i][j - 1][k];
										
										/*case Direction.Z_P:
											adj = mixes[i][j][k + 1];
										*/
										case Direction.Z_N:
											adj = mixes[i][j][k - 1];
										
									}
									
									gm.share(adj, 3);
								}
							}
							
							if( gm.get_changed() )
								gm.recalc();
					}
				}
			}
		}
	}
	
	/**
	 * Used for process()
	 * @param	gm	current mixture
	 * @return	bitflag with directions
	 */
	private function getAdjacentMixes(x:Int, y:Int, z:Int):Int {
		
		var answer:Int = Direction.ZERO;
		
		if ( x > 0 )
			answer = answer | Direction.X_N;
		if ( x < sizeX - 1 )
			answer = answer | Direction.X_P;
			
		if ( y > 0 )
			answer = answer | Direction.Y_N;
		if ( y < sizeY - 1 )
			answer = answer | Direction.Y_P;
			
		if ( z > 0 )
			answer = answer | Direction.Z_N;
		if ( z < sizeZ - 1 )
			answer = answer | Direction.Z_P;
		return answer;
	}
	
	/**
	 * Debug info about some mixture
	 * @param	x
	 * @param	y
	 * @param	z
	 * @return string with mix params
	 */
	public function d_info(x:Int, y:Int, z:Int):String {
		return mixes[x][y][z].d_output();
	}
	
	/**
	 * adds 30 KJ to custom mixture
	 * @param	x
	 * @param	y
	 */
	public function d_shit( x, y, z ):Void {
		mixes[x][y][z].addHeat(30000);
	}
	
	public function GASPIN(pos:Point3, g:Gas, amount:Float):Float
	{
		return mixes[pos.x][pos.y][pos.z].removeCustomGas(g, amount);
	}
}