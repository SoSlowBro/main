package game.atmo;
import openfl.errors.*;
import Direction;
import openfl.geom.Point;
import openfl.Lib;
/**
 * ...
 * @author Bo
 */
class GasController2D
{
	private static var SwitchDirs:Array<Int> = [Direction.X_N, Direction.Y_N];
	// checkin time << 1;
	
	private var mixes:Array<Array<GasMix>>;
	private var sizeX:Int;
	private var sizeY:Int;
	
	/**
	 * Constructor for gas controller
	 * @param	sizeX	amount of mixtures by X coordinate
	 * @param	sizeY	amount of mixtures by Y coordinate
	 */
	public function new(sizeX:Int = 500, sizeY:Int = 500) //TODO replace args with normal points.
	{
		mixes = [];
		this.sizeX = sizeX; this.sizeY = sizeY;
		var airPreset:GMPreset = new GMPreset(GMPreset.MainPresets.DEFAULT);
		var spacePreset:GMPreset = new GMPreset(GMPreset.MainPresets.SPACE);
		
		if (sizeX < 1)
			throw new ArgumentError("sizeX can't be less than 1");
		if (sizeY < 1)
			throw new ArgumentError("sizeY can't be less than 1");
		
		for (i in 0...sizeX)
		{
			mixes[i] = [];
			for (j in 0...sizeY) 
			{
				mixes[i].push( new GasMix() );
				mixes[i][j].setPreset( (Math.random() > 0.5 ? airPreset : spacePreset) );
			}
		}
	}
	
	/**
	 * Performs main calculation between stored mixtures.
	 */
	public function process():Void {
		var bitflag:Int;
		var adj:GasMix = null;
		var gm:GasMix = null;
		for (i in 0...sizeX)
		{
			for (j in 0...sizeY) 
			{
				gm = mixes[i][j];
				if ( gm.get_changed() && ! gm.get_blocked() )
				{
					bitflag = getAdjacentMixes(i, j);
					
						for (dir in SwitchDirs) //TODO Заменить или описать
						{
							if (dir & bitflag > 0) {
								switch dir
								{
									// checkin time << 1;
									/*case Direction.X_P:
										adj = mixes[i + 1][j];
									*/
									case Direction.X_N:
										adj = mixes[i - 1][j];
									
									/*case Direction.Y_P:
										adj = mixes[i][j + 1];
									*/
									case Direction.Y_N:
										adj = mixes[i][j - 1];
										
								}
								
								gm.share(adj, 2);
							}
						}
						
						if( gm.get_changed() )
							gm.recalc();
				}
			}
		}
	}
	
	/**
	 * Used for process()
	 * @param	gm	current mixture
	 * @return	bitflag with directions
	 */
	private function getAdjacentMixes(x:Int, y:Int):Int {
		
		var answer:Int = Direction.ZERO;
		
		if ( x > 0 )
			answer = answer | Direction.X_N;
		if ( x < sizeX - 1 )
			answer = answer | Direction.X_P;
			
		if ( y > 0 )
			answer = answer | Direction.Y_N;
		if ( y < sizeY - 1 )
			answer = answer | Direction.Y_P;
			
		return answer;
	}
	
	/**
	 * Debug info about some mixture
	 * @param	x
	 * @param	y
	 * @return	string with all params
	 */
	public function d_info(x:Int, y:Int):String {
		return mixes[x][y].d_output();
	}
	
	/**
	 * adds 20 kJ to custom mixture
	 * @param	x
	 * @param	y
	 */
	public function d_shit( x:Int, y:Int ):Void {
		mixes[x][y].addHeat(20000);
	}
}