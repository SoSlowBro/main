package game.atmo;

import game.atmo.GMPreset;
import openfl.errors.IllegalOperationError;
import openfl.Lib;

/**
 * Gas mixture class
 * Preforms atomic manipulations and contatins minimum data
 * 
 * If you want add new gas, put his NAME in enum Gas, add it again in GasNames:Array (it contains human-friendly names)
 * And don't forget about change GMPreset.
 * @author Bo
 */

enum Gas
{
	O2;
	CO2;
	N2;
	N2O;
	TOXINS;
}

class GasMix
{
	// PV = nR (tE * heatCap) [no entropy there]
	public static inline var R:Float = 8.314;
	
	public static inline var MIN_MOLES_TO_PROCESS:Float = 0.0001; // 1 mmol
	public static inline var MIN_JOULES_TO_PROCESS:Float = 0.0001; // 1 mJ
	
	private static var Gases:Array<Gas>;
	private static var GasNames:Array<String> = [ "Oxygen", "Carbon dioxide", "Nitrous", "Nitrous oxide", "Toxins"];
		// i dont even know, why this exists.
		
	private static function getGasHeatCap(g:Gas):Float
	{
		var hc = 0.0;
		switch (g) 
		{
			case Gas.O2:
				hc = 29.4;
			case Gas.CO2:
				hc = 37;
			case Gas.N2O:
				hc = 38.5;
			case Gas.N2:
				hc = 29.1;
			case Gas.TOXINS:
				hc = 150;
		}
		return hc;
	}
	
	//Main mixture params
	private var gasAmount:Array<Float> = [0.0, 0.0, 0.0, 0.0, 0.0 ];
	private var thermalEnergy:Float = 0; // in Joules
	private var volume:Float = 1;
	private var blocked(get, null):Bool = false;
	
	// Make it more speedy
	private var changed(get, null):Bool = false; // little (and main) speed improvment
	// Another speed improvment
	private var arch_heatCap:Float = 0;
	private var arch_temp:Float = 0;
	private var arch_press:Float = 0;
	private var arch_moles:Float = 0;
	
	/**
	 * Constructor for gas mixture.
	 * Note, that some varaibles will be not initialized, so you must use setPreset()
	 */
	public function new() 
	{
		if (Gases == null)
			Gases = Gas.createAll();
			
	}
	
	/**
	 * Addition to constructor. Initializes inner variables with custom numbers.
	 * @param	preset	If null, standart preset will be used.
	 */
	public function setPreset(preset:GMPreset):Void
	{	
		changed = true;
		
		gasAmount = preset.gasAmount.copy();
		blocked = preset.blocked;
		volume = preset.volume;
		thermalEnergy = preset.temperature * getHeatCap();
		// uh oh
		recalc();
		changed = true;
	}
	
	/**
	 * Getter for heat capacity.
	 * Use recalc() to improve speed of that getter.
	 * @return Current heat capacity
	 */
	public function getHeatCap():Float {
		if (!changed)
			return arch_heatCap;
			
		var cap:Float = 0;
		for (gas in Gases) 
			cap += gasAmount[ gas.getIndex() ] * getGasHeatCap(gas);
			
		return arch_heatCap = cap;
	}
	
	/**
	 * Getter for thermal energy.
	 * Use recalc() to improve speed of that getter.
	 * @return Current thermal energy in J )
	 */
	public function getThermEnergy():Float {
		return thermalEnergy;
	}
	
	/**
	 * Getter for temperature.
	 * Use recalc() to improve speed of that getter.
	 * @return	Current temperature
	 */
	public function getTemperature():Float {
		if (! changed)
			return arch_temp;
			
		return arch_temp = thermalEnergy / getHeatCap();
	}
	
	/**
	 * Getter for pressure.
	 * Use recalc() to improve speed of that getter.
	 * @return	Current pressure in kPa
	 */
	public function getPressure():Float {
		if (! changed)
			return arch_press;
		return arch_press = getMoles() * R * getTemperature() / volume;
	}
	
	/**
	 * Getter for moles of gas in mix.
	 * Use recalc() to improve speed of that getter.
	 * @return	Moles of *all* gases.
	 */
	public function getMoles():Float {
		if (! changed)
			return arch_moles;
			
		var amount:Float = 0;
		for (gas in Gases) 
			amount += gasAmount[ gas.getIndex() ];
		return arch_moles = amount;
	}
	
	/**
	 * Removes custom amount of moles.
	 * Affects all gases, so you should use removeCustomGas() for another manipulations
	 * Blocked mixture will not be affected, use unblock() first.
	 * @param	amount	Moles of decrease
	 * @return	New gas mixture - result of substraction
	 */
	public function removeByAmount(amount:Float):GasMix {
		var sum:Float = getMoles();
		var removed:GasMix = new GasMix();
		amount = amount < sum ? amount : sum;
		
		for (i in 0...Gases.length) 
		{
			removed.gasAmount[i] = gasAmount[i] / sum * amount;
			if ( removed.gasAmount[i] < MIN_MOLES_TO_PROCESS )
				removed.gasAmount[i] = 0;
			if ( ! blocked ) 
			{
				gasAmount[i] -= removed.gasAmount[i];
				
				if ( gasAmount[i] < MIN_MOLES_TO_PROCESS )
					gasAmount[i] = 0;
			}
		}
		removed.thermalEnergy = thermalEnergy / sum * amount;
		if (! blocked )
		{
			thermalEnergy -= removed.thermalEnergy;
			changed = true;
		}
		return removed;
	}
	
	/**
	 * Removes gases by ratio.
	 * Affects all gases, so you should use removeCustomGas() for custom manipulations 
	 * Blocked mixture will not be affected, use unblock() first.
	 * @param	ratio	Its absolute value must be in [0, 1] or it will be 1.
	 * @return	New gas mixture - result of substraction
	 */
	public function removeByRatio(ratio:Float):GasMix {
		var removed:GasMix = new GasMix();
		ratio = Math.abs(ratio);
		ratio = ratio < 1 ? ratio : 1;
		
		for (i in 0...Gases.length) 
		{
			removed.gasAmount[i] = gasAmount[i] * ratio;
			if ( removed.gasAmount[i] < MIN_MOLES_TO_PROCESS )
				removed.gasAmount[i] = 0;
			
			if (! blocked )
			{
				gasAmount[i] -= removed.gasAmount[i];
				
				if ( gasAmount[i] < MIN_MOLES_TO_PROCESS )
					gasAmount[i] = 0;
			}
		}
		removed.thermalEnergy = thermalEnergy * ratio;
		if (! blocked )
		{
			thermalEnergy -= removed.thermalEnergy;
			changed = true;
		}
		return removed;
	}
	
	/**
	 * Mimic all parameters to [mix]
	 * Blocked mixture will not be affected, use unblock() first.
	 * @param	mix	Source of new params
	 */
	private function copyFrom(mix:GasMix):Void {
		if ( blocked )
			return;
		for (i in 0...Gases.length)
			gasAmount[i] = mix.gasAmount[i];
			
		thermalEnergy = mix.thermalEnergy;
		changed = true;
	}
	
	/**
	 * Addition between mixtures. Affects only current mixture.
	 * Blocked mixture will not be affected, use unblock() first.
	 * @param	mix	Mix that should be added.
	 */
	private function merge( mix:GasMix ):Void {
		if ( blocked )
			return;
			
		if (mix.getMoles() < MIN_MOLES_TO_PROCESS
			&& mix.getThermEnergy() < MIN_JOULES_TO_PROCESS)
			return;
			
		for (i in 0...Gases.length)
			if ( mix.gasAmount[i] <= MIN_MOLES_TO_PROCESS )
				gasAmount[i] += mix.gasAmount[i];
			
		thermalEnergy += mix.thermalEnergy;
		changed = true;
	}
	
	/**
	 * Substraction between mixtures. Affects only current mixture.
	 * Blocked mixture will not be affected, use unblock() first.
	 * @param	mix Mix that should be removed.
	 */
	private function substract( mix:GasMix ):Void {
		
		if ( blocked )
			return;
			
		if (mix.getMoles() < MIN_MOLES_TO_PROCESS
			&& mix.getThermEnergy() < MIN_JOULES_TO_PROCESS)
			return;
			
		for (i in 0...Gases.length)
		{
			if ( mix.gasAmount[i] <= MIN_MOLES_TO_PROCESS )
			{
				gasAmount[i] -= mix.gasAmount[i];
				if ( gasAmount[i] <= MIN_MOLES_TO_PROCESS )
					gasAmount[i] = 0;
			}
		}
		
		thermalEnergy -= mix.thermalEnergy;
		changed = true;
	}
	
	/**
	 * Like substraction, but does not affect any mixture.
	 * 
	 * Note: if mixA.CO2 - mixB.CO2 < 0, its moles would be 0.
	 * @param	mix	Mixture to compare.
	 * @return	Comparing result.
	 */
	private function getDiff( mix:GasMix ):GasMix {
		
		var diff:GasMix = new GasMix();
		
		for (i in 0...Gases.length)
		{
			diff.gasAmount[i] = gasAmount[i] - mix.gasAmount[i];
			
			if ( diff.gasAmount[i] < MIN_MOLES_TO_PROCESS )
				diff.gasAmount[i] = 0;
		}
			
		diff.thermalEnergy = thermalEnergy - mix.thermalEnergy;
		return diff;
	}
	
	/**
	 * Adding custom amount of kJ to mixture.
	 * Blocked mixture will not be affected, use unblock() first.
	 * @param	amount	in Joules
	 */
	public function addHeat( amount:Float ):Void {
		if ( blocked )
			return;
		thermalEnergy += amount;
		changed = true;
	}
	
	/**
	 * Removes custom amount of gas.
	 * Blocked mixture will not be affected, use unblock() first.
	 * @param	gas	Use GasMix.Gas enum.
	 * @param	amount	In moles.
	 */
	public function removeCustomGas( gas:Gas, amount:Float ):Float {
		if ( blocked )
			return amount;
			
		gasAmount[ gas.getIndex() ] -= amount;
		
		if (gasAmount[ gas.getIndex() ] < 0)
		{
			var t = Math.abs(gasAmount[ gas.getIndex() ]);
			gasAmount[ gas.getIndex() ] = 0;
			return t;
		}
		
		changed = true;
		
		return amount;
	}
	
	/**
	 * Addes custom amount of gas.
	 * Blocked mixture will not be affected, use unblock() first.
	 * @param	gas	Use GasMix.Gas enum.
	 * @param	amount	In moles.
	 */
	public function addCustomGas( gas:Gas, amount:Float ):Void {
		if ( blocked )
			return;
		gasAmount[ gas.getIndex() ] += amount;
		changed = true;
	}
	
	/**
	 * Performs sharing between mixes.
	 * Blocked mixture will not be affected, use unblock() first.
	 * @param	mix	Mix to share with.
	 * @param	divider Custom divider. Used to slow exchange in simulation.
	 * @return	True if exchange was successful, false otherwise
	 */
	public function share( mix:GasMix, divider:Float ):Bool {
		if (blocked)
			return false;
			
		var delta:GasMix = getDiff( mix );
		
		// ohoho
		if ( delta.getMoles() < MIN_MOLES_TO_PROCESS && delta.getThermEnergy() < MIN_JOULES_TO_PROCESS )
			return false;
		delta = delta.removeByRatio( 1 / divider );
		// yeah
		if ( delta.getMoles() < MIN_MOLES_TO_PROCESS && delta.getThermEnergy() < MIN_JOULES_TO_PROCESS )
			return false;
		
		substract( delta );
		mix.merge( delta );
		return true;
	}
	
	/**
	 * Recalculates all archived parameters such as temperature or pressure.
	 * Use it after exchaging to improve getters speed.
	 */
	public function recalc():Void {
		getMoles();
		getPressure();
		getTemperature();
		getPressure();
		changed = false;
	}
	
	/**
	 * Returns almost full copy of current mix.
	 */
	public function copy():GasMix
	{
		var g:GasMix = new GasMix();
		
		g.gasAmount = gasAmount.copy();
		g.thermalEnergy = thermalEnergy;
		g.volume = volume;
		g.blocked = blocked;
		g.changed = true;
		
		g.recalc();
		
		return g;
	}
	
	/**
	 * Blocks mixture
	 */
	public function block():Void {
		blocked = true;
	}
	
	/**
	 * Unblocks mixture
	 */
	public function unblock():Void {
		blocked = false;
	}
	
	/**
	 * Getter for blocked flag.
	 * @return blocked value
	 */
	public function get_blocked():Bool 
	{
		return blocked;
	}
	
	/**
	 * Getter for changed flag
	 * @return changed value
	 */
	public function get_changed():Bool 
	{
		return changed;
	}
	
	/**
	 * Debug mixture output.
	 */
	public function d_output():String {
		var s = "";
		for (i in 0...Gases.length)
		{
			s += GasNames[i] + ": " + Std.string( gasAmount[i] ) + "\n";
		}
		s += "pres: " + Std.string( getPressure() ) + "\n";
		s += "temp: " + Std.string( getTemperature() ) + "\n";
		
		return s;
	}
}