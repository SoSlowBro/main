package game.atmo;

/**
 * Class-helper for GasMix. Use it in function GasMix.setPreset()
 * @author Bo
 */
enum Volume
{
	CANISTER;
	CELL;
	TANK;
	BALOON;
}
enum Contents
{
	NOTHING;
	
	PURE_TOXINS;
	PURE_CO2;
	PURE_O2;
	PURE_N2;
	PURE_N2O;
	
	AIR;
	BOMB;
}

enum Pressure
{
	ZERO;
	ATMO_1;
	ATMO_10;
	ATMO_25;
}

enum Temperature
{
	ABSOLUTE_ZERO;
	CELSIUS_ZERO;
	CELSIUS_20;
	CELSIUS_5000;
}

enum MainPresets
{
	DEFAULT;
	SPACE;
	B_SPACE;
	BOILING_TOXINS_TANK;
	BALOON;
	HONKY_BALOON;
}

class GMPreset
{	
	private static inline var S_PRESS:Float = 101.325;	// kPa
	private static inline var S_0TEMP:Float = 273.15;	// 0 grades C
	private static inline var S_AIR_O2PERC:Float = 0.21;		// * 100%
	private static inline var S_AIR_N2PERC:Float = 0.79;		// * 100%
	
	private static var Presets:Array<GMPreset> = [];
	
	public var gasAmount:Array<Float>;
	public var blocked:Bool;
	public var temperature:Float;
	public var volume:Float;
	
	public function new(?mp:MainPresets)
	{
		if (mp == null)
			mp = MainPresets.DEFAULT;
		switch (mp) 
		{
			case MainPresets.SPACE:
				setStats(Volume.CELL, Contents.NOTHING, Pressure.ZERO, Temperature.ABSOLUTE_ZERO);
			case MainPresets.B_SPACE:
				setStats(Volume.CELL, Contents.NOTHING, Pressure.ZERO, Temperature.ABSOLUTE_ZERO, true);
			case MainPresets.DEFAULT:
				setStats(Volume.CELL, Contents.AIR, Pressure.ATMO_1, Temperature.CELSIUS_20);
			case MainPresets.BOILING_TOXINS_TANK:
				setStats(Volume.TANK, Contents.PURE_TOXINS, Pressure.ATMO_25, Temperature.CELSIUS_5000);
			case MainPresets.BALOON:
				setStats(Volume.BALOON, Contents.PURE_N2O, Pressure.ATMO_10, Temperature.CELSIUS_20);
			case MainPresets.HONKY_BALOON:
				setStats(Volume.CANISTER, Contents.BOMB, Pressure.ATMO_25, Temperature.CELSIUS_5000);
		}
	}
	
	private function setStats
	(
		?vol:Volume,
		?cont:Contents,
		?pres:Pressure,
		?temp:Temperature,
		?block:Bool = false
	)
	{
		if (vol == null)
			vol  = Volume.CELL;
		if (cont == null)
			cont = Contents.AIR;
		if (pres == null)
			pres  = Pressure.ATMO_1;
		if (temp == null)
			temp = Temperature.CELSIUS_20;
		if (block == null)
			block = false;
			
		blocked = block;
		
		// Volume
		switch (vol)
		{
			case Volume.CANISTER:
				volume = 20000;
			case Volume.CELL:
				volume = 1000;
			case Volume.TANK:
				volume = 10;
			case Volume.BALOON:
				volume = 1;
		}
		
		//Pressure
		var p = 0.0;
		switch (pres) 
		{
			case Pressure.ATMO_1:
				p = S_PRESS;
			case Pressure.ATMO_10:
				p = S_PRESS * 10;
			case Pressure.ATMO_25:
				p = S_PRESS * 25;
			case Pressure.ZERO:
				p = 0.0;
		}
		
		// Temperature
		switch (temp) 
		{
			case Temperature.ABSOLUTE_ZERO:
				temperature = 0;
			case Temperature.CELSIUS_ZERO:
				temperature = S_0TEMP;
			case Temperature.CELSIUS_20:
				temperature = S_0TEMP + 20;
			case Temperature.CELSIUS_5000:
				temperature = S_0TEMP + 5000;
		}
		
		// Moles
		var mol = p * volume / (GasMix.R * temperature);
		// Check GasMix.GasNames
		switch (cont) 
		{
			case Contents.NOTHING:
				gasAmount = [0.0, 0.0, 0.0, 0.0, 0.0 ];
			case Contents.PURE_O2:
				gasAmount = [ mol, 0.0, 0.0, 0.0, 0.0 ];
			case Contents.PURE_CO2:
				gasAmount = [ 0.0, mol, 0.0, 0.0, 0.0 ];
			case Contents.PURE_N2:
				gasAmount = [ 0.0, 0.0, mol, 0.0, 0.0 ];
			case Contents.PURE_N2O:
				gasAmount = [ 0.0, 0.0, 0.0, mol, 0.0 ];
			case Contents.PURE_TOXINS:
				gasAmount = [ 0.0, 0.0, 0.0, 0.0, mol ];
			
			case Contents.AIR:
				gasAmount = [mol * S_AIR_O2PERC , 0.0, mol * S_AIR_N2PERC, 0.0, 0.0 ];
			case Contents.BOMB:
				gasAmount = [mol / 3 , 0.0, 0.0, 0.0, mol / 3 * 2 ];
		}
	}
	/**
	 * Hard way to set this.
	 * @param	vol	Use enum Volume.
	 * @param	cont	Use enum Contents.
	 * @param	pres	Use enum Pressure.
	 * @param	temp	Use enum Temperature.
	 * @param	block	Just for init block of gas mixture. Blocked mix will be infinite source of gas or infinite "gas consumer" for adjacent mixes
	 */
	public static function create
	(
		?vol:Volume,
		?cont:Contents,
		?pres:Pressure,
		?temp:Temperature,
		?block:Bool = false
	)
	{
		
		var t:GMPreset = new GMPreset();
		
		t.setStats(vol, cont, pres, temp, block);
		
		return t;
	}
	
}