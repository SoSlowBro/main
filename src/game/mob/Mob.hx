package game.mob;

import game.atmo.GasMix;
import game.Region;
import TemporarySupport;
/**
 * ...
 * @author Bo
 */
class Mob
{
	var reg(get, null):Region;
	private var pos(get, null):Point3;
	private var testInfo(get, null):String;
	private var health(get, null):Int;
	
	
	
	public function new(reg:Region) 
	{
		this.reg = reg;
		health = 100;
		testInfo = "AM ALIEV!";
		pos.x = pos.y = pos.z = 20;
	}
	
	public function gasp()
	{
		var r = reg.provideGases(this, GasMix.Gas.O2, 20);
	}
	
	public function process()
	{
		
	}
	
	public function get_pos():Point3 
	{
		return pos;
	}
	public function get_health():Int 
	{
		return health;
	}
	public function get_testInfo():String 
	{
		return testInfo;
	}
	public function get_reg():Region 
	{
		return reg;
	}
	
}