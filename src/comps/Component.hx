package comps;

/**
 * ...
 * @author Bo
 */
class Component
{
	private var msgQueue:Array<Dynamic>;
	public function new() 
	{
		msgQueue = [];
	}
	public function receive(msg:Dynamic):Void
	{
		msgQueue.push(msg);
	}
	public function process():Void
	{
		// handle msg processing here.
		// or in subclasses
	}
	
}