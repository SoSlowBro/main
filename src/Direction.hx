package;

/**
 * ...
 * @author Bo
 */
class Direction
{
	
	public static inline var ERR:Int = -1;
	public static inline var ZERO:Int = 0;
	public static inline var X_N:Int = 1;
	public static inline var X_P:Int = 2;
	public static inline var Y_N:Int = 4;
	public static inline var Y_P:Int = 8;
	public static inline var Z_N:Int = 16;
	public static inline var Z_P:Int = 32;
	
	public static var cardinal:Array<Int> = D2;
	
	public static var D2:Array<Int> = [ X_N, X_P, Y_N, Y_P];
	public static var D3:Array<Int> = [ X_N, X_P, Y_N, Y_P, Z_N, Z_P];
	
	public static function getOpposite(d:Int):Int {
		var ans:Int = ERR;
		switch (d) 
		{
			case X_P:
				ans = X_N;
			case X_N:
				ans = X_P;
			case Y_P:
				ans = Y_N;
			case Y_N:
				ans = Y_P;
			case Z_P:
				ans = Z_N;
			case Z_N:
				ans = Z_P;
		}
		return ans;
	}
}