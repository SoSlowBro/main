package;
import haxe.io.Error;
import haxe.PosInfos;
import openfl.errors.IllegalOperationError;
import Date;
import openfl.Lib;
/**
 * 
 * @author Bo
 */
class Logger
{
	
	private static function message(lvl:LogLevel, msg:String, ?info:PosInfos) {
		var fullstr:String = "";
		fullstr += '[${Date.now()}] ';
		fullstr += '$lvl: ';
		fullstr += msg;
		if (lvl.getIndex() > WARNING.getIndex())
		{
			fullstr += ' [File ${info.fileName}] ${info.className}:';
			fullstr += '${info.methodName}:${info.lineNumber}';
			if ( info.customParams != null )
			{
				fullstr += "\n";
				fullstr += 'Parameters: ${info.customParams}';
			}
		}
		//haha
		Lib.trace(fullstr); //TODO replace with normal file.
	}
	public static function log(msg:Dynamic):Void
	{
		if (msg != null)
			message(INFO, Std.string(msg) );
	}
	public static function assert(expression:Bool, ?info:PosInfos):Void
	{
		if (! expression)
			message(LogLevel.ASSERT, "Assertion failed at", info);
	}
	
}


enum LogLevel {
	NONE;
	
	INFO;
	WARNING;
	
	ERROR;
	RUNTIME;
	ASSERT;
	DEBUG;
}