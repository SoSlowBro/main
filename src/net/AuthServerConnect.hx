package net;
import sys.net.Host;
import sys.net.Socket;

/**
 * ...
 * @author Bo
 */
class AuthServerConnect
{
	var port(get, never):UInt;
	var sock:Socket;

	public function new(port:UInt) 
	{
		this.port = port;
		sock = new Socket();
		sock.connect( Host.localhost, port );
		
		sock
	}
	
	function get_port():UInt 
	{
		return port;
	}
	
}