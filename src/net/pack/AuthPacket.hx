package net.pack;

/**
 * ...
 * @author Bo
 */
enum Header
{
	Connect;
	Ack;
	AuthRequest;
	AuthResponse;
}
typedef Body = {
	var login:String;
	var token:String;
}
class AuthPacket
{
	var header:Header;
	var login:String;

	public function new(header:Header, login:String) 
	{
		this.header = header;
		
	}
	
}