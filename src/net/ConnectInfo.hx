package net;
import sys.net.Socket;

/**
 * ...
 * @author Bo
 */

enum MainStatus
{
	ALIVE;
	DISCONECTED;
	PENDING;
	BLOCKED;
}
class ConnectInfo
{	
	var mainSocket(get, null):Socket;
	var status(get, null):MainStatus;
	
	public function new(sock:Socket) 
	{
		status = ALIVE;
		mainSocket = sock;
	}
	
	function get_status():MainStatus 
	{
		return status;
	}
	function get_socket():Socket 
	{
		return mainSocket;
	}
}