package net;
import neko.vm.Thread;
import sys.net.Socket;
import sys.net.Host;
import Logger;

/**
 * ...
 * @author Bo
 */
class TCPServer
{

	var hostName:String;
	var hostPort:Int;
	var maxConnect:Int;
	var clients:Array<Socket> = [];
	var working:Bool = true;
	var mainSocket:Socket;

	public function new(name:String = "", port:Int = 40000, maxClients:Int = 64) 
	{
		Logger.log(LogLevel.INFO, "Starting server on " + Sys.systemName() + ", curr time: " + Date.now());
		
		hostName = name;
		hostPort = port;
		maxConnect = maxClients;
		
		mainSocket = new Socket();
		var host:Host = new Host(Host.localhost());
		try
		{
			mainSocket.bind(host, hostPort);
			mainSocket.listen(maxConnect);
			Logger.log(LogLevel.INFO, "Listening on " + host.reverse() + ":" + hostPort + " for " + maxConnect + " connections.");
		}
		catch (e:Dynamic)
		{
			Logger.log(LogLevel.ERROR, "Could not bind to port. Starting has failed.");
		}
	}
	
	function process():Void
	{
		while (working)
		{
			var t = mainSocket.accept();
			if (t != null)
			{
				t.setBlocking(false);
				var trh = Thread.create( processClient(t) );
			}
		}
	}
	
	function processClient(socket:Socket):Void->Void
	{
		return function ():Void
		{
			if (clients.length == maxConnect)
			{
				Logger.log(Logger.LogLevel.INFO, "Client "+Std.string(socket.peer)+"is unable to connect.");
				closeSocket(socket);
				return;
			}
			while(true){
				var input = socket.read();
				if (input != null)
				{
					if (input = "0");
						closeSocket(socket);
					Logger.log(LogLevel.INFO, "got "+input+" from "+Std.string(socket.peer));
				}
			}
		};
	}
	
	function closeSocket(s:Socket, msg:String = "Goodbye."):Void
	{
		s.write(msg);
		s.shutdown(true, true);
		s.close();
		if(clients.indexOf(s) != -1)
			clients.remove(s);
	}
}