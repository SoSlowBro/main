package;

import game.atmo.GasController2D;
import game.atmo.GasController3D;
import haxe.Timer;
import Logger.LogLevel;
import net.NAddress;
import openfl.display.FPS;
import openfl.display.Sprite;
import openfl.display.StageScaleMode;
import openfl.events.MouseEvent;
import openfl.events.Event;
import openfl.Lib;
import openfl.net.Socket;
import openfl.text.TextField;

/**
 * ...
 * @author Bo
 */
enum Test
{
	A;
	B;
	C;
}
class Main extends Sprite 
{
	var gcs:Array<GasController2D> = [];
	var gc3:GasController3D;
	var gc2:GasController2D;

	var h:Int;
	var w:Int;
	var sw:Int = 800;
	var sh:Int = 480;
	
	var t:Int = 0;
	var tf:TextField;
	var down:Bool = false;
	
	public function new() {
		super();
		
		h = w = 300;
		Logger.log('Init with $h x $w');
		
		/*Timer.measure(function ():Void{
			gc3 = new GasController3D(300,300,4);
		});*/
		Timer.measure(function ():Void{
		//	gc2 = new GasController2D(h, w);
		});
		
		var a = 255;
		var b = 1;
		var c = 3;
		var d = 64;
		var add:NAddress = new NAddress( (a << 24) | (b << 16) | (c << 8) | d , 1024);
		Logger.assert(add.getA() == a);
		Logger.assert(add.getB() == b);
		Logger.assert(add.getC() == c);
		Logger.assert(add.getD() == d);
		Logger.log(add);
		
		var sock:Socket = new Socket();
		
		sock.connect();
		
		
		tf = new TextField();
		tf.textColor = 0x333333;
		tf.selectable = false;
		tf.width = 500;
		tf.height = 15;
		
		addChild(tf);
		stage.addChild(new openfl.display.FPS(10, 10, 0xffffff));
		
		addEventListener(MouseEvent.MOUSE_DOWN, doabarrelroll);
		addEventListener(MouseEvent.MOUSE_UP, doabarrelroll);
		stage.addEventListener(MouseEvent.CLICK, handleClick);
		//addEventListener(Event.ENTER_FRAME, process);
	}
	
	private function handleClick(e:MouseEvent):Void 
	{
		//Sys.exit(0);
		
		Logger.log(e);
	}
	
	private function doabarrelroll(e:MouseEvent):Void 
	{
		down = !down;
	}
	
	private function process(e:Event):Void 
	{
		if (t % 25 == 0)
		{
			Logger.log('$t:');
			if (Math.random() > 0.5)
			{
				Lib.trace("Shitting :)");
				gc2.d_shit(w>>1, w>>1);
			}
			Timer.measure(gc2.process);
		} else {
			gc2.process();
		}
		t++;
	}
}
